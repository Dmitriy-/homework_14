﻿#include <iostream>

int main()
{
    std::cout << "Enter your name? ";
    std::string name;
    std::cin >> name;

    std::cout << "City about? ";
    std::string city;
    std::cin >> city;

    std::cout << name << " " << city <<"\n";

    std::cout << name.length() << "\n";
    std::cout << city.length() << "\n";

    std::cout << name.front() << " " << city.back() << "\n";

}

